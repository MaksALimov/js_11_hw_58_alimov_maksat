import React from 'react';
import './Alert.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const Alert = props => {
    return (
        <div className="container">
            {props.showAlert ? <div className={props.type}>
                    <div className="wrapper">
                        {props.children}
                        {props.dismiss ?
                            <button
                                onClick={props.dismiss}
                                className="close">
                                <FontAwesomeIcon
                                    icon={faTimes}/>
                            </button> : null}
                    </div>
                </div>
                : null}
        </div>
    );
};

export default Alert;