import React from 'react';
import './Modal.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const Modal = props => {
    return (
        <div className="Modal"
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateX(-100vh)',
                 opacity: props.show ? '1' : '0'
             }}>
            <h3 className="separate">
                {props.title}
                <button onClick={props.closed} className="times">{<FontAwesomeIcon icon={faTimes}/>}</button>
            </h3>
            {props.children}
            {props.modalButtons ? //Если modalButtons передаётся через пропс, то вывожу их. Если нет, то не вывожу
                props.modalButtons.map((button, i) => {
                    return <button
                        key={i}
                        onClick={button.clicked}
                        className={button.type}>
                        {button.label}
                    </button>
                }) : null
            }
        </div>
    );
};

export default Modal;