import React, {useState} from "react";
import Modal from './components/UI/Modal/Modal'
import './components/UI/Alert/Alert'
import Alert from "./components/UI/Alert/Alert";

const App = () => {
    const [show, setShow] = useState(true);

    const [showAlert, setShowAlert] = useState(true);

    const onClosed = () => {
        setShow(!show);
    };

    const onContinue = () => {
        alert('You pressed continue');
    }

    const modalButtons = [
        {type: 'Primary', label: 'Continue', clicked: onContinue},
        {type: 'Danger', label: 'Close', clicked: onClosed}
    ]

    const alertCloseHandler = () => {
        setShowAlert(!showAlert);
    };

    return (
        <>
            <Modal
                show={show}
                closed={onClosed}
                modalButtons={modalButtons}
                title="Some kinda modal title"
            >
                <p>
                    A modal is a dialog box/popup window that is displayed on top of the current page: Open Modal.
                </p>
            </Modal>
            <button onClick={onClosed} className="modal-open">Open Modal</button>
            <Alert
                type="warning" //warning, danger, success, primary
                showAlert={showAlert}
                dismiss={alertCloseHandler}
            >
               This is a warning type alert
            </Alert>
            <Alert
                type="success" //warning, danger, success, primary
                showAlert={showAlert}
            >
                This is a success type alert
            </Alert>
            <button onClick={alertCloseHandler} className="alert-open">Show Alert</button>
        </>
    )
};

export default App;
